# Camera configuration file for Build mode.
# See cameras.txt for a description of syntax.

camera      "Prerender" SmoothSims1
zoom        0 (56.0) (40.0) (20) (140)
fov           (33.0)
orientation 0 (190)
smoothing   1 1
slope       2